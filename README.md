# README

Part of test tasks.
Original code is present in commit #0 and consisted of single class only - `UserFileImportProcess`.

Task was to refactor the class.

I would like to also extract wrapping of operation with those log writing so in general `perform` method would look like:

```ruby
def perform(file_id)
    @user_file = UserFile.find(file_id)
    BgJobWrapper.execute('User import') do
      process_file
    end
  end
```
which would handle `before_perform`, `after_perform`, `perform_error` by itself, but on first sight it's not possible due to updates on `UserFile` and `SunSpot` (which actually may depend on other jobs that are implemented in the same manner).
 
                     