module UserServiceObjects
  class ForImport < Struct.new(:user)

    def self.find_or_create_user(email, register, category_id)
      user = find_or_create(email, register)
      create_subscription_if_needed(user, register, category_id)
    end

    private
    def self.find_or_create(email, register)
      user = User.find_by_email(email)
      if user
        user.update_attribute(:subscribed_to_sevastopol_news, true) if register
        return user
      end

      password = generate_password
      user = User.new(login: email,
                      password: generate_password,
                      email: email,
                      is_imported: true,
                      subscribed_to_sevastopol_news: register)

      if user.valid?
        user.skip_confirmation!
        user.save
        user.confirm!
        UserMailer.welcome_email(self, password).deliver if register
      end
      user
    end

    def self.create_subscription_if_needed(user, register, category_id)
      return unless register
      return unless AdvtCategory.where(id: category_id).exists?
      return if user.subscriptions.where(category_id: category_id).exists?

      Subscription.create(user_id: user.id, category_id: category_id)
    end

    def self.generate_password
      range = ['A'..'Z'].to_a + ['a'..'z'].to_a
      Array.new(6) { range.sample }.join
    end
  end
end