class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :login
      t.boolean :is_imported
      t.boolean :subscribed_to_sevastopol_news #...?

      t.timestamps
    end
  end
end
