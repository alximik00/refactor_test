class CreateSiteLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :site_logs do |t|
      t.string :message

      t.timestamps
    end
  end
end
