class CreateUserFiles < ActiveRecord::Migration[5.1]
  def change
    create_table :user_files do |t|
      t.string :parsing_status
      t.timestamp :started_at
      t.timestamp :finished_at
      t.string :path
      t.boolean :registrate #OMFG where did you get this word??
      t.timestamps
    end
  end
end
