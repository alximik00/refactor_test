require 'rails_helper'
require 'ostruct'

RSpec.describe UserFileImportProcess do

  def run_job(file_id)
    UserFileImportProcess.new.perform(file_id )
  end

  def run_job_lmb(file_id)
    ->{ UserFileImportProcess.new.perform(file_id ) }
  end

  it 'does not raise error' do
    file = UserFile.create
    expect( run_job_lmb(file.id) ).not_to raise_error
  end

  it 'does not create users if cant open file' do
    file = UserFile.create
    expect(run_job_lmb(file.id)).not_to change(User, :count)
  end

  context 'with data' do
    let(:file) { UserFile.create }
    let(:strings) {['email1;1', 'email2;2'] }

    before(:each) do
      User.delete_all
      Subscription.delete_all

      allow_any_instance_of(UserFile).to receive(:file) { OpenStruct.new(url: 'somewhere')  }
      @file_double = double('file')
      allow(@file_double).to receive(:each_line).and_yield(strings[0]).and_yield(strings[1])
      allow(File).to receive(:open).and_return(@file_double)
    end

    it 'creates users' do
      expect( run_job_lmb(file.id) ).to change(User, :count).by(2)
    end

    it 'puts data from file to users' do
      run_job(file.id )

      expect( User.first.email ).to eq('email1')
      expect( User.last.email ).to eq('email2')
    end

    context 'user' do
      it 'is created if not yet exists' do
        expect( run_job_lmb(file.id) ).to change(User, :count).by(2)
      end

      it 'isnt create if exists' do
        User.create(email: 'email1')
        User.create(email: 'email2')
        expect( run_job_lmb(file.id) ).not_to change(User, :count)
      end

      it 'sets attribute of subscription for user if needed' do
        file =  UserFile.create(registrate: true)
        run_job(file.id )
        expect( User.last.subscribed_to_sevastopol_news ).to be_truthy
      end
    end

    context 'subscription' do
      let(:file) { UserFile.create(registrate: true) }
      let!(:category) { AdvtCategory.create }

      before(:each) do
        allow(@file_double).to receive(:each_line).
            and_yield("email1;#{category.id}").
            and_yield("email2;#{category.id}")
        allow(File).to receive(:open).and_return(@file_double)
      end

      it 'is created if category found' do
        expect( run_job_lmb(file.id) ).to change(Subscription, :count).by(2)
      end

      it 'isnt created if category not found' do
        allow(AdvtCategory).to receive(:where).and_return( OpenStruct.new('exists?' => false))
        expect( run_job_lmb(file.id) ).not_to change(Subscription, :count)
      end

      it 'isnt created if subscription already exists' do
        User.create(email: 'email1').subscriptions.create(category_id: category.id)
        User.create(email: 'email2').subscriptions.create(category_id: category.id)
        expect( run_job_lmb(file.id) ).not_to change(Subscription, :count)
      end
    end

    describe 'logs and result' do
      before(:each) do
        SiteLog.delete_all
      end
      it 'writes log on start' do
        run_job(file.id)
        expect( SiteLog.first.message ).to match(/начат/)
      end

      it 'writes log on end' do
        run_job(file.id)
        expect( SiteLog.last.message ).to match(/окончен/)
      end

      it 'writes log on error' do
        allow_any_instance_of(User).to receive(:save) { raise StandardError.new('on demand') }
        run_job(file.id)
        expect( SiteLog.where('message like "Ошибка%"').count ).to eq(1)
      end
    end
  end
end