class UserFileImportProcess

  include Sidekiq::Worker
  sidekiq_options :queue => :critical, :retry => false

  def perform(file_id)
    @user_file = UserFile.find(file_id)
    @count = 0
    before_perform
    begin
      process_file
      after_perform
    rescue StandardError => exception
      perform_error( exception)
    end
  end

  private
  
  def process_file
    file_url = @user_file.file.url
    file = File.open(UrlFileReader.read(file_url), 'r:windows-1251')
    file.each_line { |line| process_line(line) }
  end

  def process_line(line)
    data = line.split(';')
    email = data[0].strip.downcase
    return if ValidatesEmailFormatOf::validate_email_format(email).present?

    UserServiceObjects::ForImport.find_or_create_user(email, @user_file.registrate?, data[1])
    @count += 1
  end

  def before_perform
    SiteLog.create(message: "Импорт пользователей №#{@user_file.id} начат", created_at: Time.now)
    @user_file.update_attributes({parsing_status: 'parsing', started_at: Time.zone.now})
  end

  def after_perform
    Sunspot.commit_if_dirty
    @user_file.update_attributes({parsing_status: 'finished', finished_at: Time.zone.now})
    SiteLog.create(message: "Импорт пользователей №#{@user_file.id} окончен, обработано #{@count} записей", created_at: Time.now)
  end

  def perform_error(exception)
    SiteLog.create(message: "Ошибка при импорте пользователей №#{@user_file.id}, #{exception}", created_at: Time.now)
    @user_file.update_attribute(:parsing_status, 'error')
  end
end