class User < ApplicationRecord

  attr_accessor :password

  has_many :subscriptions

  def skip_confirmation!
  end

  def confirm!
  end
end
